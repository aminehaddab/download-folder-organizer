import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from main import organizeFolder

class MyHandler(FileSystemEventHandler):
    def on_created(self, event):
        if not event.is_directory:
            print(f'New file detected: {event.src_path}')
            organizeFolder(download_path)

if __name__ == "__main__":
    download_path = "C:/Users/<userName>/Downloads"  # Replace with your Downloads folder path
    event_handler = MyHandler()
    observer = Observer()
    observer.schedule(event_handler, download_path, recursive=False)
    observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()