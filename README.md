# Downloads Folder Organizer

Author: Amine Haddab

A Python script to automatically organize files in your Downloads folder based on their file extensions.

## Table of Contents

- [Introduction](#introduction)
- [Features](#features)
- [How It Works](#how-it-works)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
  - [Usage](#usage)
- [Contributing](#contributing)


## Introduction

The Downloads Folder Organizer is a Python script designed to simplify the cluttered mess that often accumulates in your computer's Downloads folder. It automatically categorizes and moves files into specific folders based on their file extensions, making it easier to find and manage your downloaded files.

## Features

- Automatically detects new files in the Downloads folder.
- Organizes files into subfolders based on their file extensions.
- Supports customization and extension mapping.

## How It Works

The script uses the following logic to organize your Downloads folder:

1. It monitors the Downloads folder for new file creations using the `watchdog` library.
2. When a new file is detected, it extracts its file extension.
3. It checks if a folder for that extension exists within the Downloads folder.
   - If the folder exists, it moves the file to the corresponding subfolder.
   - If not, it creates a new subfolder for that extension and then moves the file.

## Getting Started

### Prerequisites

Before using this script, you need to have Python installed on your system. You can download Python from the official website: [Python Downloads](https://www.python.org/downloads/)

### Installation

1. Clone or download this repository to your local machine.

```bash 
   git clone https://gitlab.com/aminehaddab/download-folder-organizer.git 
```

2. Install the required Python libraries.
```bash 
   pip install watchdog
```

### Usage

1. Put your download file path in the `auto_run.py` script.

2. Run the `auto_run.py` script to start monitoring your Downloads folder and organizing files automatically.

### Contributing
Contributions to this project are welcome. If you have ideas for improvements or new features, feel free to open an issue or submit a pull request.