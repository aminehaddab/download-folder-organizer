import os
import shutil

def organizeFolder(path):
    files = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]

    for file in files:
        filename,extension = os.path.splitext(file)
        extension = extension [1:]

        # check if the extension exists
        if os.path.exists(path+'/'+extension):
            # move the file
            if os.path.exists(path+'/'+extension+'/'+file):
                # delete the file
                try:
                    os.remove(path+'/'+extension+'/'+file)
                except:
                    print("An exception occurred")
            shutil.move(path+'/'+file, path+'/'+extension+'/'+file)
        else:
            # create the directory
            os.makedirs(path+'/'+extension)
            # move the file
            shutil.move(path+'/'+file, path+'/'+extension+'/'+file)